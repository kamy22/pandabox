import time
from config import Config


class TimeTable(object):
    max_wait = 5

    def __init__(self, storage):
        self.__storage = storage

    async def add_last_sync_time(self, client_secret_key):
        await self.__storage.save(client_secret_key, time.time())

    async def is_valid(self, client_secret_key):
        temp_time = await self.__storage.get(client_secret_key)
        return time.time() - float(temp_time) < self.max_wait

    async def are_valid(self, client_secret_keys):
        valid = True
        for client_secret in client_secret_keys:
            if not await self.is_valid(client_secret):
                valid = False
        return valid
