from server_core.serializable import Serializable

from errors import TooManyClients, InvalidTempTokenForTheConversation
import uuid
import time


class SyncTable(Serializable):
    def __init__(self, params=None):
        self.__clients = list()
        self.__connected_clients = list()
        self.__synced = False
        self.__temp_token = self.__generate_uuid64()
        self.__master_key = self.__generate_uuid64()
        self.__conversation_id = self.__generate_uuid64()
        self.__creation_time = time.time()
        self.__connection_ack = 0
        self.__destroy_time = 60

        if params:
            self.__deserialize__(params)

    def check_and_add_client(self, client_secret_key):
        clients = self.__clients

        if clients:
            if client_secret_key in clients:
                return True

            if len(clients) <= 2:
                clients.append(client_secret_key)
                return True
            raise TooManyClients

        clients.append(client_secret_key)
        return True

    def check_and_add_connected_client(self, client_secret_key, temp_token):
        clients = self.__connected_clients

        if temp_token != self.get_temp_token():
            raise InvalidTempTokenForTheConversation

        if clients:
            if client_secret_key in clients and client_secret_key in self.__clients:
                return True

            if len(clients) <= 2:
                clients.append(client_secret_key)
                return True
            raise TooManyClients

        clients.append(client_secret_key)
        return True

    def add_connection_ack(self, client_secret):
        if client_secret in self.__connected_clients:
            self.__connection_ack += 1

    def can_destroy(self):
        if self.__connection_ack == 2:
            return True

        if (time.time() - self.__creation_time) > self.__destroy_time:
            return True

        return False

    def is_valid(self, client_secret_key):
        clients = self.__clients

        if len(clients) > 2:
            raise TooManyClients

        if len(clients) < 2 or client_secret_key not in clients:
            return False

        return True

    def is_connected(self, client_secret_key):
        clients = self.__connected_clients

        if len(clients) > 2:
            raise TooManyClients

        if len(clients) < 2 or client_secret_key not in clients or client_secret_key not in self.__clients:
            return False

        return True

    def __generate_uuid64(self):
        return uuid.uuid4().hex

    def set_synced(self):
        self.__synced = True

    def is_synced(self):
        return self.__synced

    def get_sign_key(self):
        return self.__master_key

    def get_temp_token(self):
        return self.__temp_token

    def get_connected_clients(self):
        return self.__connected_clients

    def get_clients(self):
        return self.__clients

    def get_conversation_id(self):
        return self.__conversation_id
