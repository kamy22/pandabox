import json
import aioredis


class Storage(object):
    def __init__(self, namespace):
        self.__pool = None
        self.__namespace = namespace

    async def get(self, key):
        await self.__check_connection()
        with (await self.__pool) as connection:
            return await connection.get(self.__get_namespace_key(key))

    async def save(self, key, value):
        await self.__check_connection()
        with (await self.__pool) as connection:
            await connection.set(self.__get_namespace_key(key), value)

    async def delete(self, key):
        await self.__check_connection()
        with (await self.__pool) as connection:
            await connection.delete(self.__get_namespace_key(key))

    def __get_namespace_key(self, key):
        return '{}_{}'.format(self.__namespace, key)

    async def __check_connection(self):
        if not self.__pool:
            self.__pool = await aioredis.create_pool(address=('localhost', 6379), encoding='utf-8')


class ObjectStorage(object):
    def __init__(self, storage_name, object_class):
        self.__object_class = object_class
        self.__namespace = storage_name
        self.__pool = None

    async def get(self, key):
        await self.__check_connection()
        with (await self.__pool) as connection:
            load_object = await connection.get(self.__get_namespace_key(key))

        if not load_object:
            return {}

        object_params = json.loads(load_object)
        return self.__object_class(object_params)

    async def save(self, key, custom_object):
        await self.__check_connection()
        with (await self.__pool) as connection:
            await connection.set(self.__get_namespace_key(key), custom_object.to_json())

    async def delete(self, key):
        await self.__check_connection()
        with (await self.__pool) as connection:
            await connection.delete(self.__get_namespace_key(key))

    def __get_namespace_key(self, key):
        return '{}_{}'.format(self.__namespace, key)

    async def __check_connection(self):
        if not self.__pool:
            self.__pool = await aioredis.create_pool(address=('localhost', 6379), encoding='utf-8')
