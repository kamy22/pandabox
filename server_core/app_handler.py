from aiohttp import web
from errors import TooManyClients, InvalidConversation
from server_core.conversation import Conversation
from server_core.storage import Storage, ObjectStorage
from server_core.synctable import SyncTable
from server_core.timetable import TimeTable


class Handler:
    def __init__(self):
        self.timetable_storage = Storage('timetable')
        self.timetable = TimeTable(self.timetable_storage)

        self.conversations = ObjectStorage('conversations', Conversation)

        self.box = ObjectStorage('box', SyncTable)

    async def sync(self, request):
        req_info = await request.post()
        client_secret_key, pandabox_id = req_info.get('secret', None), req_info.get('pandabox_id', None)
        await self.timetable.add_last_sync_time(client_secret_key)

        synctable = await self.box.get(pandabox_id)

        if not synctable:
            synctable = SyncTable()

        try:
            synctable.check_and_add_client(client_secret_key)
        except TooManyClients:
            print('Chat collision!')
            await self.box.delete(pandabox_id)

        await self.box.save(pandabox_id, synctable)

        response = {
            'sync': False
        }

        try:
            if synctable.is_synced():

                conversation = await self.conversations.get(synctable.get_conversation_id())

                if not conversation:
                    conversation = Conversation()

                for client in synctable.get_connected_clients():
                    conversation.add_user(client)

                await self.conversations.save(synctable.get_conversation_id(), conversation)

                response = {
                    'sync': True,
                    'temp_token': synctable.get_temp_token(),
                    'conversation_id': synctable.get_conversation_id(),
                    'sign_key': synctable.get_sign_key()
                }
            elif synctable.is_valid(client_secret_key) and await self.timetable.are_valid(synctable.get_clients()):
                synctable.set_synced()
                await self.box.save(pandabox_id, synctable)
                response = {
                    'sync': True,
                    'temp_token': synctable.get_temp_token()
                }
        except TooManyClients:
            print('Chat collision!')
            await self.box.delete(pandabox_id)

        return web.json_response(data=response)

    async def confirm_sync(self, request):
        req_info = await request.post()
        client_secret_key, pandabox_id, temp_token = req_info.get('secret', None), req_info.get('pandabox_id', None), \
                                                     req_info.get('temp_token', None)

        synctable = await self.box.get(pandabox_id)

        if not synctable:
            synctable = SyncTable()

        try:
            synctable.check_and_add_connected_client(client_secret_key, temp_token)
        except TooManyClients:
            print('Chat collision!')
            await self.box.delete(pandabox_id)

        await self.box.save(pandabox_id, synctable)

        response = {
            'connected': False
        }

        try:
            if synctable.is_connected(client_secret_key):
                synctable.add_connection_ack(client_secret_key)
                await self.box.save(pandabox_id, synctable)
                if synctable.can_destroy():
                    await self.box.delete(pandabox_id)
                response = {
                    'connected': True
                }
        except TooManyClients:
            print('Chat collision!')
            await self.box.delete(pandabox_id)

        return web.json_response(data=response)

    async def save_message(self, request):
        req_info = await request.post()
        conversation_id, message = req_info.get('conversation_id', None), req_info.get('message', None)
        client_secret_key = req_info.get('secret', None)

        conversation = await self.conversations.get(conversation_id)
        if not conversation:
            raise InvalidConversation

        if client_secret_key not in conversation.get_users():
            return web.json_response(data={}, status=400)

        conversation.add_message(message)
        await self.conversations.save(conversation_id, conversation)
        return web.json_response(data=conversation.get_messages())

    async def get_messages(self, request):
        conversation_id = request.match_info['id']
        req_info = await request.post()
        client_secret_key = req_info.get('secret', None)

        conversation = await self.conversations.get(conversation_id)
        if not conversation:
            return web.json_response(data=[], status=404)

        if client_secret_key not in conversation.get_users():
            return web.json_response(data={}, status=400)

        return web.json_response(data=conversation.get_messages())
