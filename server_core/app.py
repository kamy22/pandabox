import asyncio
from aiohttp import web
from server_core.app_handler import Handler

handler = Handler()
app = web.Application()
app.router.add_route('POST', '/panda_sync', handler.sync)
app.router.add_route('POST', '/panda_sync/confirm', handler.confirm_sync)
app.router.add_route('POST', '/panda_messages', handler.save_message)
app.router.add_route('POST', '/panda_messages/{id}', handler.get_messages)