import json
from errors import InvalidParamsForDeserialization


class Serializable(object):
    def to_json(self):
        temp_dict = dict()

        for key in self.__dict__:
            temp_dict[key] = getattr(self, key)

        return json.dumps(temp_dict)

    def __deserialize__(self, params):
        for key in params:
            if hasattr(self, key):
                setattr(self, key, params[key])
            else:
                raise InvalidParamsForDeserialization
