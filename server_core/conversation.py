import uuid
from server_core.serializable import Serializable
import json


class Conversation(Serializable):
    def __init__(self, params=None):
        self.__users = list()
        self.__messages = list()

        if params:
            self.__deserialize__(params)

    def check_user(self, client_secret_key):
        return client_secret_key in self.__users

    def is_full(self):
        return len(self.__users) > 2

    def add_user(self, client_secret_key):
        if client_secret_key not in self.__users:
            self.__users.append(client_secret_key)

    def add_message(self, message):
        self.__messages.append(message)

    def get_messages(self):
        return self.__messages

    def get_users(self):
        return self.__users
