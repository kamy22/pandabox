class Config(object):
    def __init__(self, hostname, namespace):
        super(Config, self).__init__()
        self.HOST = hostname
        self.PASP = namespace
        self.SYNCPATH = '/panda_sync'
        self.SYNCPATH_CONFIRM = '/panda_sync/confirm'
        self.MESSAGESPATH = '/panda_messages'
        self.MAX_REQUESTS = 40
        self.MAX_WAIT_TIME = 2

    def get_host(self):
        return self.HOST

    def get_pasp(self):
        return self.PASP

    def get_syncpath(self):
        return self.SYNCPATH

    def get_syncpath_confirm(self):
        return self.SYNCPATH_CONFIRM

    def get_max_requests(self):
        return self.MAX_REQUESTS

    def get_max_wait_time(self):
        return self.MAX_WAIT_TIME

    def get_messages_path(self):
        return self.MESSAGESPATH
