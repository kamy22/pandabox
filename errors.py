class TooManyClients(Exception):
    pass


class InvalidTempTokenForTheConversation(Exception):
    pass


class InvalidConversation(Exception):
    pass


class InvalidParamsForDeserialization(Exception):
    pass
