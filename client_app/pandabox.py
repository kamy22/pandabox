import aiohttp
import time
import uuid
import sys
from sys import stdin
from Crypto.Cipher import AES
import base64


class PandaBox(object):
    """PandaBox object used to start the applications.

    Represents the client part of the PandaBox Project. It allows users to contact
    the server, sync the connection and start the conversation.

    """
    def __init__(self, config):
        """PandaBox constructor method.

        Configures the PandaBox environment setting all the needed private attributes.
        It also show the conversation_id to start the conversation from the other client.

        """
        self._config = config
        self._sk = self.__generate_secret_uuid()
        self._signkey = self._conversation_id = None
        self._first_time = True

        if len(sys.argv) > 1:
            self._pandabox_id = sys.argv[1]
        else:
            self._pandabox_id = self.__generate_secret_uuid()
            print('Your pandabox_id is %s' % self._pandabox_id)

    async def start(self):
        """Starts the PandaBox application. It needs to be run in an asyncio loop."""
        await self.sync_panda()
        await self.show_chat()

    async def sync_panda(self):
        """Loop. Synchronizes the client with the other client.

        The flow is complex. At the beginning, the client sends a sync request to the server. After saving the
        client_secret and the conversation_id for the client, the server responds.

        Server response sync is true when the clients are sync (added in synctable and satisfy the sync time).
        In this case, the PandaBox application gets and saves the received parameters in order to use them for the
        future communication.

        After storing data, the client informs the server about the successful sync and the GUI can be loaded.

        """

        counter = 0
        print('Syncing...')

        while True:
            response_gen = await self.__send_sync_request()
            r = await response_gen.json()

            if r.get('sync', {}) == True:
                temp_sync_token, self._key_token = r.get('temp_token'), r.get('key_token')
                self._signkey = r.get('sign_key')
                self._conversation_id = r.get('conversation_id')

                response_gen = await self.__confirm_sync_request(temp_sync_token)
                response = await response_gen.json()

                if response.get('connected', {}) == True:
                    break

            counter += 1

            if counter % self._config.get_max_requests() == 0:
                time.sleep(5)
            else:
                time.sleep(1)

    async def save_message(self, message):
        """Sends a message to the server and retreive the messages list of the conversation.

        Args:
            message (str): The message to encrypt

        Returns:
            List[string]: List of encrypted messages

        """
        message = self.__encrypt_message(message)
        response_gen = await self.__save_message(str(message))
        messages = await response_gen.json()
        return messages

    async def get_messages(self):
        """Gets the conversation messages.

        Returns:
            List[str]: List of encrypted messages

        """
        response_gen = await self.__get_messages()
        messages = await response_gen.json()
        return messages

    def show_messages(self, messages):
        """Decrypts messages and show the Pandabox GUI."""

        print('\n\n')
        print('-------------------------------------')
        print('--------------- PANDABOX ------------')
        print('-------------------------------------')
        for m in messages:
            print('- ' + self.__decode_message(m))
        print('-------------------------------------')

    async def show_chat(self):
        """Starts the Pandabox GUI."""
        while True:
            if self._first_time:
                print('Welcome into your PandaBox ----------')
                print('To refresh only press [Enter] key ---')
                print('-------------------------------------')
                print('Enter your message: -----------------')
                self._first_time = False
            else:
                print('Enter your message: -----------------')
            message = stdin.readline()

            if len(message) == 1:
                messages = await self.get_messages()
            else:
                messages = await self.save_message(message)
            self.show_messages(messages)

    async def __save_message(self, message):
        """Compiles the secret data for the request and forward a message to the server.

        Args:
            message (str): The message to send

        Returns:
            List[str]: List of all messages

        """
        r_info = {
            'conversation_id': self._conversation_id,
            'message': message,
            'secret': self._sk
        }

        return await aiohttp.post(self._config.get_host() + self._config.get_messages_path(), data=r_info)

    async def __get_messages(self):
        """Compiles the secret data for the request and get the conversation messages from the server.

        Returns:
            List[str]: List of all messages

        """
        r_info = {
            'secret': self._sk
        }

        return await aiohttp.post(self._config.get_host() + self._config.get_messages_path() + \
                                  '/' + self._conversation_id, data=r_info)

    async def __send_sync_request(self):
        """Compiles the secret data and sends a sync request to the server.

        Returns:
            Dict: Status params

        """
        r_info = {
            'secret': self._sk,
            'pandabox_id': self._pandabox_id
        }

        return await aiohttp.post(self._config.get_host() + \
                                  self._config.get_syncpath(), data=r_info)

    async def __confirm_sync_request(self, temp_token):
        """Compiles the secret data and sends a confirmation sync to the server.

        Returns:
            Dict: Status params

        """
        r_info = {
            'secret': self._sk,
            'pandabox_id': self._pandabox_id,
            'temp_token': temp_token
        }

        return await aiohttp.post(self._config.get_host() + self._config.get_syncpath_confirm(), data=r_info)

    def __generate_secret_uuid(self):
        """Compiles the secret data and sends a confirmation sync to the server.

        Note:
            To implement a strong function!

        Returns:
            string: The generated uuid

        """
        return uuid.uuid4()

    def __manipulate_message_for_16(self, message):
        """Generates a 16 char multiple length message.

        Very important function. For AES encryption all messages have to be 16 char multiple length.

        Returns:
            str: 16 char multiple length message

        """
        length = len(message.encode('utf-8'))
        if length > 16:
            to_add = 16 - (length % 16)
            to_add = ' ' * to_add
        else:
            to_add = 16 - length
            to_add = ' ' * to_add

        return message + to_add

    def __encrypt_message(self, message):
        """Encrypts a message using AES encryption + base 64 encoding.

        Args:
            message (str): The message to encrypt

        Returns:
            string: The encrypted message

        """
        encryption_suite = AES.new(self._signkey[0:24], AES.MODE_CBC, 'This is an IV456')
        message = self.__manipulate_message_for_16(message)

        return base64.b64encode(encryption_suite.encrypt(message)).decode('utf-8')


    def __decode_message(self, message):
        """Decrypts a message using AES decrypt + base 64 decode.

        Args:
            message (str): The message to encrypt

        Returns:
            string: The decrypted message

        """
        decryption_suite = AES.new(self._signkey[0:24], AES.MODE_CBC, 'This is an IV456')
        return decryption_suite.decrypt(base64.b64decode(message.encode('utf-8'))).strip().decode('utf-8')
