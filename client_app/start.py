import asyncio
from client_app.pandabox import PandaBox
from config import Config
import time

config = Config('http://pandabox.molendys.com:2222', 'test')
application = PandaBox(config)

loop = asyncio.get_event_loop()
loop.run_until_complete(application.start())