# Setup Environment
# ------------------------------------------------------------

# Init
# ------------------------------------------------------------
apt-get update -y
apt-get install -y build-essential
apt-get install -y tcl8.5

# Redis
# ------------------------------------------------------------
echo "Installing Redis ..."
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make MALLOC=libc

sudo cp src/redis-server /usr/local/bin/
sudo cp src/redis-cli /usr/local/bin/

# Python3.5
# ------------------------------------------------------------
echo "Installing Python 3.5 ..."
cd
sudo apt-get install -y libssl-dev openssl
wget https://www.python.org/ftp/python/3.5.1/Python-3.5.1.tgz
tar xzvf Python-3.5.1.tgz 
cd Python-3.5.1/
./configure 
make
make install

# Git
# ------------------------------------------------------------
echo "Installing Git ..."
apt-get install -y git

# Creating aiohttp application
# ------------------------------------------------------------
cd
mkdir app
cd app

pyvenv-3.5 --without-pip venv
source venv/bin/activate
curl https://bootstrap.pypa.io/get-pip.py | python3.5
deactivate
source venv/bin/activate

pip install gunicorn
pip install -e git+https://github.com/KeepSafe/aiohttp.git#egg=aiohttp

#git clone git@bitbucket.org:kamy22/pandabox.git .
#pip install -r requirements.txt
#export PYTHONPATH=$PYTHONPATH:`pwd`
#cd server_core/
#gunicorn app:app --bind 127.0.0.1:80 --worker-class aiohttp.worker.GunicornWebWorker
